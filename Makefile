include config.mk

lxc-hub: clean
	@hugo --quiet
	@uglifyjs -o public/index.js public/index.js
	@yui-compressor public/style.css > public/style.min.css
	@mv public/style.min.css public/style.css
	@sed -e 's/^[ \t]*//g' -e '/^ *$$/d' public/index.html > public/index.min.html
	@cat public/index.min.html | tr -d '\n' > public/index.html
	@rm public/index.min.html
	@rm -f public/index.xml
	@rm -f public/sitemap.xml
	@rm -fr public/categories
	@rm -fr public/tags
	@cd public && for file in *; do [ -d "$$file" ] && mv "$$file/index.html" "_$$file" && rm -fr  "$$file" && mv "_$$file" "$$file"; done
	@mkdir -p lxc-hub
	@mv public lxc-hub
	@cp -f lxc-hub.cf lxc-hub
	@rsync -Pavurdzq lxc-hub root@vps-chambaz.duckdns.org:/mnt/data
	@ssh root@vps-chambaz.duckdns.org ln -fs /mnt/data/lxc-hub/lxc-hub.cf /etc/nginx/sites-enabled/lxc-hub
	@ssh root@vps-chambaz.duckdns.org systemctl reload nginx
	@ssh root@vps-chambaz.duckdns.org certbot run -n --nginx -q -d lxc-hub.com
	@rm -fr lxc-hub


clean:
	@rm -fr lxc-hub
	@rm -fr public

