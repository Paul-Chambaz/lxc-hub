# Lxc Hub

## Description

A website to host linux container packages. It allows to quicly take advantage of lxc-install and use it to install linux containers already set up.
The linux container packages are just a toml holding information about the container and an ansible playbook for the container configuration. For more information please look at [the lxc-install repository](https://gitlab.com/Paul-Chambaz/lxc-install).

If you are the official maintainer of a program and you want to provide a linux container package and for it to be officially supported for lxc, please get in touch with [me](mailto:paul.chambaz@tutanota.com).
This would allow some containers to be official containers and therefore provide a guarantee that the container is correctly set up.

## How to contribute

As said before, this project only for works for now with [the lxc-install program](https://gitlab.com/Paul-Chambaz/lxc-install).
However, this program is closer to a script that a big software, since the configuration is really simple.

Linux container packages rely on at least two files, a config.toml file holds information about the container and variables for the ansible playbook.
That means it you can provide default value for the container while making it very easy to modify even important information about the container for individuals.
You will also need to configure an ansible playbook for the container. The objective of this ansible playbook is to configure the container itself, not the host.
This means you should configure on the container itself as much as you can and then leave the configuration of reverse proxies, domain names, and file architecture for the host.
You can, of course, add many more files and have an ansible playbook as complex as you like, just make sure to put those files on the same directory as the playbook.

Once you have made a linux container package, you will need to [contact me](mailto:paul.chambaz@tutanota.com) and after I have tested that the container you have provided is working, I will add it to the website.
As said earlier, if you are the official maintainer of a project, your project will be declared as official.

To get your linux container package on this website, you will need to provide the following in an email called **Linux Container Package: your package**.
```
# the package name
title = "package_title"
# the version >= 1.0.0
version = "package_version"
# a short (< 150 characters) description
description = "package_description"
# the default distribution
distribution = "package_distribution"
# the default release
release = "package_release"
# the default architecture
architecture = "package_architecture"
# the git repository holding the package
repository = "package_git_repository"
# the categories (files/system/security/communication/home/entertainment/social/web) (max 3)
category = [ "package_category1", ... ]
```

Please also provide me with a link to your personal git page and your personal website. Although not required, this will allow me to make sure you can be trusted and accelerate the process of adding your linux container package to this website.

## Installation

This project requires hugo for building and an remote repository to publish the container.
This website also uses nginx for configuration.

If you have all three, make sure you have root access to your server and run
```
make
```
This should take care of everything.

However, this site is not configured to be used by anyone else, so you will most likely have to modify the config.mk file to properly configure the installation.
That being said, feel free to use this project for your own need.

## License

This project is licensed under the GPLv3. If you want more information, read the license file.
