function copyToClipboard(copy, id) {
  navigator.clipboard.writeText(copy);
  btn = document.getElementById(id);
  btn.innerText = "Copied";
  setTimeout(function() {
    btn.innerText = "Copy repo"
  }, 1000);
} 

function formatNumber(n) {
  if (n == 1) {
    return 1;
  } else if (n < 1000) {
    return n;
  } else if (n < 10 * 1000) {
    return Math.floor(n / 100) / 10 + "k";
  } else if (n < 1000 * 1000) {
    return Math.floor(n / 1000) + "k";
  } else if (n < 10 * 1000 * 1000) {
    return Math.floor(n / 100000) / 10 + "m";
  } else if (n < 1000 * 1000 * 1000) {
    return Math.floor(n / 1000000) + "m";
  } else if (n < 10 * 1000 * 1000 * 1000) {
    return Math.floor(n / 100000000) / 10 + "b";
  } else {
    return Math.floor(n / 1000000000) + "b";
  }
}

function timeago(lastmod) {
  var now = Date.now();
  var diff = (now - lastmod) / 1000;
  if (diff < 60) {
    return "just now";
  } else if (diff < 60 * 2) {
    return "1 minute ago";
  } else if (diff < 60 * 60) {
    var mins = Math.floor(diff / 60);
    return mins + " minutes ago";
  } else if (diff < 60 * 60 * 2) {
    return "1 hour ago";
  } else if (diff < 60 * 60 * 24) {
    var hours = Math.floor(diff / (60 * 60));
    return hours + " hours ago";
  } else if (diff < 60 * 60 * 24 * 2) {
    return "1 day ago";
  } else if (diff < 60 * 60 * 24 * 7) {
    var days = Math.floor(diff / (60 * 60 * 24));
    return days + " days ago";
  } else if (diff < 60 * 60 * 24 * 7 * 2) {
    return "1 week ago";
  } else if (diff < 60 * 60 * 24 * 30) {
    var weeks = Math.floor(diff / (60 * 60 * 24 * 7));
    return weeks + " weeks ago";
  } else if (diff < 60 * 60 * 24 * 30 * 2) {
    return "1 month ago";
  } else if (diff < 60 * 60 * 24 * 365) {
    var months = Math.floor(diff / (60 * 60 * 24 * 30));
    return months + " months ago";
  } else if (diff < 60 * 60 * 24 * 365 * 2) {
    return "1 year ago";
  } else {
    var years = Math.floor(diff / (60 * 60 * 24 * 365));
    return years + " years ago";
  }
}

function selectFilter(id) {
  div = document.getElementById("si" + id);
  div.classList.toggle("selected");
}

function searchbar() {
  const search = document.getElementById("search");
  const containers = document.getElementsByClassName("container");
  console.log(containers.length);
  console.log(containers);
  search.addEventListener("input", () => {
    const searchText = search.value.toLowerCase().trim().normalize('NFD');
    console.log(searchText);
    // document.write("<p>search: " + "[" + searchText + "]</p>")
  });
  // const searchText = search.value.toLowerCase().trim).normalize('NFD').replace(/\p{Diacritic}/gu, "")

  // document.addEventListener("DOMContentLoaded", () => {
  //   for (e of document.getElementsByClassName("js-only")) {
  //     e.classList.remove("js-only");
  //   }
  //   const recipes = document.querySelectorAll("#artlist li");
  //   const search = document.getElementById("search");
  //   const oldheading = document.getElementById("newest-recipes");
  //   const clearSearch = document.getElementById("clear-search");
  //   const artlist = document.getElementById("artlist");
  //   search.addEventListener("input", () => {
  //     const searchText = search.value.toLowerCase().trim().normalize('NFD').replace(/\p{Diacritic}/gu, "");
  //     const searchTerms = searchText.split(" ");
  //     const hasFilter = searchText.length > 0;
  //     artlist.classList.toggle("list-searched", hasFilter);
  //     oldheading.classList.toggle("hidden", hasFilter);
  //     recipes.forEach(recipe => {
  //       const searchString = `${recipe.textContent} ${recipe.dataset.tags}`.toLowerCase().normalize('NFD').replace(/\p{Diacritic}/gu, "");
  //       const isMatch = searchTerms.every(term => searchString.includes(term));
  //       recipe.hidden = !isMatch;
  //       recipe.classList.toggle("matched-recipe", hasFilter && isMatch);
  //     })
  //   }) clearSearch.addEventListener("click", () => {
  //     search.value = "";
  //     recipes.forEach(recipe => {
  //       recipe.hidden = false;
  //       recipe.classList.remove("matched-recipe");
  //     }) artlist.classList.remove("list-searched");
  //     oldheading.classList.remove("hidden");
  //   })
  // })
}
