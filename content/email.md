---
title: "Email"
official: true
version: "1.0.0"
description: "Postfix/Dovecot email server"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-email"
category: [ "communication" ]
---
