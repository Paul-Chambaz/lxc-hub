---
title: "Bitwarden"
official: false
version: "1.0.0"
description: "Open source password management solutions for individuals, teams, and business organizations"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-bitwarden"
category: [ "security" ]
---
