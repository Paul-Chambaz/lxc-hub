---
title: "Matrix"
official: false
version: "1.0.0"
description: "A new basis for open, interoperable, decentralised real-time communication"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-matrix"
category: [ "communication" ]
---
