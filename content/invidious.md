---
title: "Invidious"
official: false
version: "1.0.0"
description: "Invidious is an alternative front-end to YouTube"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-invidious"
category: [ "entertainment" ]
---
