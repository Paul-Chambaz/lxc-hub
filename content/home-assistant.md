---
title: "Home Assistant"
official: false
version: "1.0.0"
description: "Open source home automation that puts local control and privacy first"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-home-assistant"
category: [ "home" ]
---
