---
title: "Git"
official: false
version: "1.0.0"
description: "Git is a fast, scalable, distributed revision control system"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-git"
category: [ "system" ]
---
