---
title: "Mastodon"
official: false
version: "1.0.0"
description: "Your self-hosted, globally interconnected microblogging community"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-mastodon"
category: [ "social" ]
---
