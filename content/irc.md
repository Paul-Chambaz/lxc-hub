---
title: "IRC"
official: false
version: "1.0.0"
description: "IRC server"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-irc"
category: [ "communication" ]
---
