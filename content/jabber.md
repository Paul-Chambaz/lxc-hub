---
title: "Jabber"
official: false
version: "1.0.0"
description: "Robust, Ubiquitous and Massively Scalable Messaging Platform"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-jabber"
category: [ "communication" ]
---
