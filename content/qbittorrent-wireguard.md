---
title: "qBittorrent Wireguard"
official: false
version: "1.0.0"
description: "qBittorrent configured with wireguard"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-qbittorrent-wireguard"
category: [ "files" ]
---
