---
title: "Samba"
official: false
version: "1.0.0"
description: "Samba is the standard Windows interoperability suite of programs for Linux and Unix"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-samba"
category: [ "system" ]
---
