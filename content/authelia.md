---
title: "Authelia"
official: false
version: "1.0.0"
description: "The Single Sign-On Multi-Factor portal for web apps"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-authelia"
category: [ "security" ]
---
