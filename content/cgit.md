---
title: "Cgit"
official: false
version: "1.0.0"
description: "A hyperfast web frontend for git repositories written in C"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-cgit"
category: [ "system" ]
---
