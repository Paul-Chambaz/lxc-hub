---
title: "qBittorrent"
official: false
version: "1.0.0"
description: "qBittorrent BitTorrent client"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-qbittorrent"
category: [ "files" ]
---
