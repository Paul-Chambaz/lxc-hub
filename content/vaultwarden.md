---
title: "Vaultwarden"
official: false
version: "1.0.0"
description: "Unofficial Bitwarden compatible server written in Rust"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-vaultwarden"
category: [ "security" ]
---
