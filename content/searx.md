---
title: "Searx"
official: false
version: "1.0.0"
description: "Privacy-respecting metasearch engine"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-searx"
category: [ "web" ]
---
