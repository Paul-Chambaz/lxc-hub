---
title: "Jellyfin"
official: false
version: "1.0.0"
description: "The Free Software Media System"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-jellyfin"
category: [ "entertainment" ]
---
