---
title: "Photoprism"
official: false
version: "1.0.0"
description: "AI-Powered Photos App for the Decentralized Web"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-photoprism"
category: [ "cloud" ]
---
