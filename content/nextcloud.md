---
title: "Nextcloud"
official: false
version: "1.0.0"
description: "Nextcloud server, a safe home for all your data"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 256
repository: "https://gitlab.com/Paul-Chambaz/lxc-nextcloud"
category: [ "cloud" ]
---

