---
title: "Jitsi"
official: false
version: "1.0.0"
description: "Secure, Simple and Scalable Video Conferences that you use as a standalone app or embed in your web application"
distribution: "alpine"
release: "edge"
architecture: "amd64"
downloads: 1815
repository: "https://gitlab.com/Paul-Chambaz/lxc-jitsi"
category: [ "entertainment" ]
---
